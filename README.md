![Logo UNET](unetLogo.png "Logo de la UNET")

"Sistema Bancario"
===============

### Práctica Parcial I de Programación I (Java)

Práctica del primer parcial de Programación. Consiste en un sistema bancario programado en lenguaje Java.

Proyecto desarrollado en NetBeans.

#### Enunciado
El banco desea agregar un tipo de cuenta que registre los pagos de cuotas efectuados por los clientes que tienen préstamo. Cada vez que un cliente desea solicitar un prestamo, el operador del sistema abre una cuenta para el préstamo cuyo saldo inicial es el monto del mismo. Cuando el cliente dese cancelar un cuota, se registra una nueva transaccion en la cuenta del préstamo de tipo "pago de cuota". La cuenta de préstamo solo acepta este tipo de transacción. Modifique el programa dado en clase para agregar esta nueva caracteristica al programa.

Por otro lado, el banco se ha dado cuenta de un bug en el sistema  a la hora de realizar transferencias. Escriba el codigo necesario para corregirlo.


#### Desarrolladores
* [Anny Chacón (@AnnyChacon)](https://github.com/AnnyChacon)
* [Gregory Sánchez (@gregsanz182)](https://github.com/gregsanz182)

#### Asignatura
* Nombre: Programación I
* Código: 0416202T
* Profesor: Manuel Sánchez

*Proyecto desarrollado con propositos educativos para la **Universidad Nacional
Experimental del Táchira***
