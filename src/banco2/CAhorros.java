package banco2;

//
//
//  Generated by StarUML(tm) Java Add-In
//
//  @ Project : Untitled
//  @ File Name : CAhorros.java
//  @ Date : 31/01/2013
//  @ Author : qwerty
//
//




public class CAhorros extends Cuenta {
    @Override
    public void agregarTransaccion(Transaccion trans) {
        if(trans instanceof Cheque){
            System.out.println("La transaccion cobro de chque no esta permitida");
        }else{
            float saldoAct = this.saldoActual + trans.getMonto();
            
            if(saldoAct<0){
                System.out.println("Saldo insuficiente para realizar la transaccion");
            }else{
                saldoAct = saldoAct;
                transacciones.add(trans);
                System.out.println("Transaccion realizar correctamente");
            }
        }
    }
    
    @Override
    public String toString() {
        return "  **  Cuenta de ahorros  **"+super.toString();
    }
}
