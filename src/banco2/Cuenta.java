package banco2;

//

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

//
//  Generated by StarUML(tm) Java Add-In
//
//  @ Project : Untitled
//  @ File Name : Cuenta.java
//  @ Date : 31/01/2013
//  @ Author : qwerty
//
//




public abstract class Cuenta {
    protected String titular;
    protected float saldoIncial;
    protected float saldoActual;
    protected String numeroCuenta;
    protected ArrayList<Transaccion> transacciones;  // vector dinamico
    public abstract void agregarTransaccion(Transaccion trans);
    public Cuenta() {
        transacciones = new ArrayList<>();
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public float getSaldoActual() {
        return saldoActual;
    }

    public void setSaldoActual(float saldoActual) {
        this.saldoActual = saldoActual;
    }

    public float getSaldoIncial() {
        return saldoIncial;
    }

    public void setSaldoIncial(float saldoIncial) {
        this.saldoIncial = saldoIncial;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public ArrayList<Transaccion> transacciones getTransacciones() {
        return transacciones;
    }

   
    @Override
    public String toString() {
        return "Numero de Cuenta:"+numeroCuenta+"\nTitular :"+titular+"\nSaldo Actual: "+saldoActual; 
    }
    
    public void mostrarTransacciones() {
        for(int i=0;i<transacciones.size();i++){
            System.out.println((i+1)+": "+transacciones.get(i));
        }    
    }
    
    public void pedirDatos() {
    BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
    
        System.out.println("Titular: ");
        titular = readLine();
        System.out.println("Saldo Inicial: ");
        saldoIncial = Float.parseFloat(read.Line());
        
    }
}
